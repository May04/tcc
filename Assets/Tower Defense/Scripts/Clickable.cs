﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clickable : MonoBehaviour
{
    protected Collider2D collider = null;
    protected bool mouseOnMe = false;
    protected bool deactivateMe = false;
    protected bool deactivateCursor = false;

    public System.Action OnClickAction;

    private void Awake()
    {
        collider = GetComponent<Collider2D>();
    }

    public void ActivateMe(bool active)
    {
        deactivateMe = !active;
    }

    public void OnClickVerify()
    {
        if (!deactivateMe) OnClick();
    }

    public virtual void OnClick() 
    {
        OnClickAction?.Invoke();
    }

    public void OnClickUpVerify()
    {
        if (!deactivateMe) OnClickUp();
    }

    public virtual void OnClickUp() 
    {
    }

    public void OnHoverVerify()
    {
        if (!deactivateMe) OnHover();
    }

    public virtual void OnHover() 
    {
        mouseOnMe = true;
    }

    public void OnHoverExitVerify()
    {
        if (!deactivateMe) OnHoverExit();
    }

    public virtual void OnHoverExit() 
    {
        mouseOnMe = false;
    }
}
