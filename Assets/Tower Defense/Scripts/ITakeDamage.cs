﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITakeDamage
{
    void TakeDamage(float damage, float armorPenetration, bool tower = false);
}
