﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    [SerializeField]
    private PlayerStats stats = null;
    [SerializeField]
    private int initialDiamonds = 0;
    [SerializeField]
    private int initialHearts = 0;
    [SerializeField]
    private int initialWaves = 0;
    [SerializeField]
    private EndLevelWindow endScreen = null;
    [SerializeField]
    private string endLevelDescription = null;
    [SerializeField]
    private int oneStarWavesGreaterThan = 0;
    [SerializeField]
    private int TwoStarWavesGreaterThan = 0;
    [SerializeField]
    private int ThreeStarWavesGreaterThan = 0;
    [SerializeField]
    private EnemySpawnPoint[] spawnPoints = null;
    [Header("LevelData")]
    [SerializeField]
    private LevelData levelData = null;
    [SerializeField]
    private LevelData levelToUnlockData = null;
    private EnemyBase[] enemies = null;
    [Header("Evaluation Feedback")]
    [SerializeField]
    private int minDiffTowers = 0;
    [SerializeField]
    private int minNumTowers = 0;
    [SerializeField]
    private float minPositioningScore = 0;
    [SerializeField]
    private float minMeleeSurrounding = 0;
    [SerializeField]
    private float minTowerDamage = 0;
    [SerializeField]
    private float minPowerHit = 0;
    private bool gameEnded = false;
    private Evaluation evaluation = null;

    private void Awake()
    {
        stats.Diamonds = initialDiamonds;
        stats.Hearts = initialHearts;
        stats.TotalWaves = initialWaves;
        stats.Waves = 0;
    }

    private void Start()
    {
        evaluation = FindObjectOfType<Evaluation>();
        StartCoroutine(VerifyEndGame());
    }

    private void Update()
    {
        if (stats.Hearts <= 0) EndLevel();
    }

    private IEnumerator VerifyEndGame()
    {
        yield return new WaitForSeconds(2f);

        int finishedSpawns = 0;
        foreach(EnemySpawnPoint spawnPoint in spawnPoints)
        {
            if(spawnPoint.Completed) finishedSpawns++;
        }

        enemies = FindObjectsOfType<EnemyBase>();

        if (finishedSpawns == spawnPoints.Length && enemies.Length == 0)
        {
            EndLevel();
            yield break;
        }

        StartCoroutine(VerifyEndGame());
    }

    private void EndLevel() 
    {
        if (gameEnded) return;

        gameEnded = true;

        Time.timeScale = 0;

        endScreen.SetDescription(endLevelDescription);

        int rating = 0;

        if (stats.Hearts > ThreeStarWavesGreaterThan)
        {
            endScreen.SetThreeStar();
            rating = 3;
        }
        else if (stats.Hearts > TwoStarWavesGreaterThan)
        {
            endScreen.SetTwoStar();
            rating = 2;
        }
        else if (stats.Hearts > oneStarWavesGreaterThan)
        {
            rating = 1;
            endScreen.SetOneStar();
        }
        else
        {
            endScreen.SetNoStar();
            endScreen.SetDescription("Você falhou na missão. Tente novamente.");
        }

        if (evaluation != false)
        {
            if (evaluation.AddTowerCount < minNumTowers) endScreen.AddTip("Tente adicionar mais torres!");
            if (evaluation.DiffTowersCount < minDiffTowers) endScreen.AddTip("Tente adicionar diferentes tipos de torres!");
            if (evaluation.DamageScore < minTowerDamage && evaluation.CoinsUsed > 0) endScreen.AddTip("Você está gastanto mais ouro do que o necessário!");
            if (evaluation.PowersHit < minPowerHit) endScreen.AddTip("Tente atingir mais inimigos com os seus poderes!");
            if (evaluation.PositioningScore < minPositioningScore) endScreen.AddTip("Tente melhorar o posicionamento das torres!");
            if (evaluation.UsedSoldier && evaluation.MeleeSurrondingScore < minMeleeSurrounding) endScreen.AddTip("Tente cercar os soldados com torres de ataque a distância!");

            evaluation.Life = stats.Hearts;
        }

        endScreen.gameObject.SetActive(true);

        if (rating == 0) endScreen.PlayLoseSound();
        else endScreen.PlayWinSound();

        if(rating > levelData.Rating) levelData.Rating = rating;
        if(levelToUnlockData != null && rating > 0) levelToUnlockData.Unlocked = true;

        levelData.SaveValues();
    }
}
