﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class TutorialEvent : MonoBehaviour
{
    [SerializeField]
    private Clickable clickableToListen = null;
    [SerializeField]
    private Button buttonToListen = null;
    public System.Action TutorialAction;
    public UnityAction action;

    private void Start()
    {
        if(clickableToListen != null )clickableToListen.OnClickAction += () => { TutorialAction?.Invoke(); };
        if (buttonToListen != null)
        {
            action += () => { TutorialAction?.Invoke(); };
            buttonToListen.onClick.AddListener(action);
        }
    }
}
