﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialManager : MonoBehaviour
{
    [SerializeField]
    private GameObject speechNext = null;
    [SerializeField]
    private List<Steps> steps = new List<Steps>();
    [SerializeField]
    private FinalStep finalStep = new FinalStep();
    private int currentStep = 0;
    private bool waitEvent = false;
    private Clickable[] clicklabes = null;
    private bool endedTutorial = false;
    void Start()
    {
        clicklabes = FindObjectsOfType<Clickable>();

        foreach (Clickable clickable in clicklabes) clickable.ActivateMe(false);

        Time.timeScale = 0;

        StartCoroutine(PlayStep());
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return) && !waitEvent && !endedTutorial)
        {
            currentStep++;
            StartCoroutine(PlayStep());
        }
    }

    private IEnumerator PlayStep()
    {
        if (currentStep >= steps.Count)
        {
            endedTutorial = true;
            foreach (GameObject go in steps[currentStep - 1].objectsToActivate) go.SetActive(false);
            foreach (Clickable ck in steps[currentStep - 1].clickablesToActivate) ck.ActivateMe(false);
            if (steps[currentStep - 1].tutorialEvent != null) steps[currentStep - 1].tutorialEvent.TutorialAction -= OnActionEvent;

            foreach (GameObject go in finalStep.objectsToActivate) go.SetActive(true);
            foreach (GameObject go in finalStep.objectsToDeactivate) go.SetActive(false);
            foreach (Clickable ck in finalStep.clickablesToActivate) ck.ActivateMe(true);
            if(finalStep.activateAllClickables)foreach (Clickable clickable in clicklabes) clickable.ActivateMe(true);

            speechNext.SetActive(false);

            Time.timeScale = 1;

            yield break;
        }

        if (currentStep > 0 && !steps[currentStep].notDeactivateLast)
        {
            foreach (GameObject go in steps[currentStep - 1].objectsToActivate) go.SetActive(false);
            foreach (Clickable ck in steps[currentStep - 1].clickablesToActivate) ck.ActivateMe(false);
            if(steps[currentStep - 1].tutorialEvent != null) steps[currentStep - 1].tutorialEvent.TutorialAction -= OnActionEvent;
        }

        yield return new WaitForSecondsRealtime(steps[currentStep].delay);

        speechNext.SetActive(steps[currentStep].showSpeechNext);
        waitEvent = !steps[currentStep].showSpeechNext;
        Time.timeScale = steps[currentStep].activateTime ? 1 : 0;

        foreach (GameObject go in steps[currentStep].objectsToActivate) go.SetActive(true);
        foreach (GameObject go in steps[currentStep].objectsToDeactivate) go.SetActive(false);
        foreach (Clickable ck in steps[currentStep].clickablesToActivate) ck.ActivateMe(true);

        if (steps[currentStep].tutorialEvent != null) steps[currentStep].tutorialEvent.TutorialAction += OnActionEvent;

        if (steps[currentStep].autoNext)
        {
            yield return new WaitForEndOfFrame();
            currentStep++;
            StartCoroutine(PlayStep());
        }
    }

    private void OnActionEvent()
    {
        currentStep++;
        StartCoroutine(PlayStep());
    }

    [System.Serializable]
    private struct Steps
    {
        public string name;
        public List<GameObject> objectsToActivate;
        public List<GameObject> objectsToDeactivate;
        public List<Clickable> clickablesToActivate;
        public TutorialEvent tutorialEvent;
        public float delay;
        public bool autoNext;
        public bool showSpeechNext;
        public bool activateTime;
        public bool notDeactivateLast;
    }

    [System.Serializable]
    private struct FinalStep
    {
        public List<GameObject> objectsToActivate;
        public List<GameObject> objectsToDeactivate;
        public List<Clickable> clickablesToActivate;
        public bool activateAllClickables;
    }
}
