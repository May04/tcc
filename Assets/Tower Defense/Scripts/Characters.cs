﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Characters : MonoBehaviour
{
    [SerializeField]
    private Image lifeBarFill = null;
    protected SpriteRenderer spriteRenderer = null;
    protected float maxLife = 0;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void LateUpdate()
    {
        spriteRenderer.sortingOrder = -Mathf.RoundToInt(spriteRenderer.bounds.min.y);
        transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.y);
    }

    protected void SetLife(float life)
    {
        if (maxLife <= 0) return;

        lifeBarFill.fillAmount = life / maxLife;
    }
}
