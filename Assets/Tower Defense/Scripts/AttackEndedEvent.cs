﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackEndedEvent : MonoBehaviour
{
    public System.Action OnAttackEndedAction;

    public void OnAttackEnded()
    {
        OnAttackEndedAction?.Invoke();
    }
}
