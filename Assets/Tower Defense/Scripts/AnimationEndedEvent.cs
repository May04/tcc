﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEndedEvent : MonoBehaviour
{
    public System.Action OnAnimationEndedAction;

    public void OnAnimationEnded()
    {
        OnAnimationEndedAction?.Invoke();
    }
}
