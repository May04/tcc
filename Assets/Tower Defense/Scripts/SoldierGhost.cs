﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoldierGhost : MonoBehaviour
{
    [SerializeField]
    private Transform soldierSpot = null;
    [SerializeField]
    private SpriteRenderer[] soldiersSprite = null;
    public Transform SoldierSpot { set { soldierSpot.position = value.position; AdjustSpites(); } }

    private void AdjustSpites()
    {
        foreach(SpriteRenderer sprite in soldiersSprite)
        {
            if (soldierSpot.transform.localPosition.y > 0) sprite.sortingOrder = -1;
            else sprite.sortingOrder = 1;
        }
    }
}
