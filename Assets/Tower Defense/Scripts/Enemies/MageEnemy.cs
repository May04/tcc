﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MageEnemy : EnemyBase
{
    [Header("Mage Enemy")]
    [SerializeField]
    private AnimationStartedEvent animationStarted = null;
    [SerializeField]
    private float cure = 0;
    [SerializeField]
    private float cureDelay = 0;
    [SerializeField]
    private LayerMask whatToCure = new LayerMask();
    private float cureSpeedTimer = 0;
    private Collider2D[] alliesToCure = null;
    private AudioSource audioSource = null;

    private void Start()
    {
        base.Start();
        audioSource = GetComponent<AudioSource>();
        animationStarted.OnAnimationStartedAction += Cure;
        cureSpeedTimer = cureDelay;
    }

    private void FixedUpdate()
    {
        base.FixedUpdate();
        cureSpeedTimer -= Time.deltaTime;
        if (cureSpeedTimer <= 0) 
        {
            if (AttackingSoldier != null) return;
            animator.SetTrigger("Cure");
            if (audioSource != null) audioSource.Play();
            cureSpeedTimer = cureDelay;
        }
    }

    private void Cure() 
    {
        alliesToCure = Physics2D.OverlapCircleAll(transform.position, range, whatToCure);

        foreach(Collider2D ally in alliesToCure)
        {
            EnemyBase allyBase = ally.GetComponent<EnemyBase>();
            if (allyBase == null) return;
            if (allyBase == this) return;
            allyBase.GetCured(cure);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }
}
