﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemyBossPower : MonoBehaviour
{
    [SerializeField]
    private AnimationStartedEvent animationStarted = null;
    [SerializeField]
    private Animator animator = null;
    [SerializeField]
    private List<GameObject> enemiesToSpawn = new List<GameObject>();
    [SerializeField]
    private List<Transform> targetPoints = new List<Transform>();

    private void Start()
    {
        animationStarted.OnAnimationStartedAction += SpawnEnemy;
    }

    public void Summon()
    {
        animator.Play("SpawnPoint");
    }

    private void SpawnEnemy()
    {
        int index = Random.Range(0, enemiesToSpawn.Count);

        EnemyBase newEnemy = Instantiate(enemiesToSpawn[index]).GetComponent<EnemyBase>();

        if (newEnemy == null) return;

        newEnemy.transform.position = transform.position;
        newEnemy.transform.rotation = targetPoints[0].rotation;
        newEnemy.NonTargetable = newEnemy.NonTargetablebySoldier = false;

        foreach (Transform target in targetPoints) newEnemy.targetPoints.Add(target);
    }
}
