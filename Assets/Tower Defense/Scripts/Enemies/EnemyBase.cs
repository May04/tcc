﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class EnemyBase : Characters, ITakeDamage
{
    [Header("Stats")]
    [SerializeField]
    private float life = 0;
    [SerializeField]
    private float speed = 0;
    [SerializeField]
    private float armor = 0;
    [SerializeField]
    private int damageToPlayerLife = 0;
    [SerializeField]
    protected float attackDamage = 0;
    [SerializeField]
    private float attackSpeed = 0;
    [SerializeField]
    protected float armorPenetration = 3;
    [SerializeField]
    protected float range = 0;
    [SerializeField]
    protected LayerMask whatIsEnemies = new LayerMask();

    [Header("Player Reward")]
    [SerializeField]
    private int moneyToGain = 0;
    [SerializeField]
    private PlayerStats playerStats = null;
    [SerializeField]
    private GameObject gainedDiamonds = null;

    [Header("Components")]
    [SerializeField]
    protected Animator animator = null;
    [SerializeField]
    private AttackEndedEvent attackEndedEvent = null;
    protected float attackSpeedTimer = 0;

    [HideInInspector]
    public bool IsBeingAttackedBySoldier = false;
    [HideInInspector]
    public Soldier AttackingSoldier = null;
    [HideInInspector]
    public List<Transform> targetPoints = new List<Transform>();

    private bool died = false;

    private Evaluation evaluation = null;

    public int DamageToPlayerLife => damageToPlayerLife;
    public int OrderInLayer { set => spriteRenderer.sortingOrder = value; }
    public bool Died => died;
    public float DistanceToEnd
    {
        get
        {
            float dist = 0;
            for(int i = 0; i < targetPoints.Count; i++)
            {
                if (i == 0) dist += Vector3.Distance(transform.position, targetPoints[i].position);
                else dist += Vector3.Distance(targetPoints[i].position, targetPoints[i - 1].position);                
            }
            return dist;
        }
    }
    
    [Header("Debug")]
    [SerializeField]
    private float currentSpeed = 0;
    [SerializeField]
    private float currentLife = 0;
    [SerializeField]
    private bool nonTargetable = true;
    [SerializeField]
    private bool nonTargetableBySoldier = true;

    public bool NonTargetable { get => nonTargetable; set => nonTargetable = value; }
    public bool NonTargetablebySoldier { get => nonTargetableBySoldier; set => nonTargetableBySoldier = value; }

    protected void Start()
    {
        evaluation = FindObjectOfType<Evaluation>();
        if(attackEndedEvent != null) attackEndedEvent.OnAttackEndedAction += AttackEnded;
        currentSpeed = speed;
        currentLife = maxLife = life;

        NonTargetable = NonTargetablebySoldier = false;
    }

    protected void FixedUpdate()
    {
        attackSpeedTimer -= Time.deltaTime;

        if (attackSpeedTimer <= 0)
        {
            Attack();
            attackSpeedTimer = attackSpeed;
        }

        if(!IsBeingAttackedBySoldier) Move();
    }

    protected virtual void Attack()
    {
        if(AttackingSoldier != null)
        {
            int rotationY = 0;
            if (transform.position.x > AttackingSoldier.transform.position.x) rotationY = 180;
            transform.rotation = new Quaternion(0, rotationY, 0, 0);

            animator.SetTrigger("Attack");
        }
    }

    protected virtual void AttackEnded()
    {
        if (AttackingSoldier == null) return;
        AttackingSoldier.TakeDamage(attackDamage, armorPenetration);
    }

    private void Move()
    {
        if (targetPoints.Count <= 0) return;
        
        if (transform.position.x == targetPoints.First().position.x && transform.position.y == targetPoints.First().position.y)
        {
            targetPoints.Remove(targetPoints.First());

            if (targetPoints.Count <= 0) return;
        }

        transform.position = Vector2.MoveTowards(transform.position, targetPoints.First().position, currentSpeed);
        transform.rotation = targetPoints.First().rotation;
    }

    public void TakeDamage(float damage, float armorPenetration, bool tower = false)
    {
        float currentArmor = armorPenetration > armor ? 0 : armor - armorPenetration;

        float deflectedDamage = (damage * currentArmor) / 100;

        float trueDamage = damage - deflectedDamage;

        currentLife -= trueDamage;

        SetLife(currentLife);

        if (tower && evaluation != null) evaluation.OnTowerDamage(trueDamage);

        if (currentLife <= 0) Die();

        StartCoroutine(DamageBlink());
    }

    public void GetCured(float cure)
    {
        currentLife = currentLife + cure > life ? life : currentLife + cure;
    }

    public virtual void HitKill()
    {
        Die();
    }

    private void Die()
    {
        died = true;
        playerStats.IncreaseDiamondsIn(moneyToGain);

        if (gainedDiamonds != null)
        {
            GainedOrLostStatIndicator sc = Instantiate(gainedDiamonds).GetComponent<GainedOrLostStatIndicator>();
            sc.transform.localPosition = transform.position;
            sc.ValuePlusText = moneyToGain;
            sc.DestroyOnDisable = true;
        }

        Destroy(this.gameObject);
    }

    private IEnumerator DamageBlink()
    {
        int i = 0;
        while (i < 3)
        {
            spriteRenderer.color = Color.red;
            yield return new WaitForSeconds(.2f);
            spriteRenderer.color = Color.white;
            yield return new WaitForSeconds(.2f);
            i++;
        }
    }

    public void Freeze(bool freeze)
    {
        if (freeze)
        {
            spriteRenderer.color = new Color(0, 0.5097919f, 1);
            currentSpeed = speed / 2;
        }
        else
        {
            currentSpeed = speed;
            spriteRenderer.color = Color.white;
        }
    }

    protected virtual void OnAttackEnd() { }
}
