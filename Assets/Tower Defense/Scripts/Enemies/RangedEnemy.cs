﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedEnemy : EnemyBase
{
    [SerializeField]
    private ProjectileShooter projectileShooter = null;

    private Collider2D[] enemiesToAttack = null;
    private Collider2D soldierToAttack = null;

    protected override void Attack()
    {
        if(AttackingSoldier != null)
        {
            Collider2D enemy = AttackingSoldier.GetComponent<Collider2D>();
            AttackEnemy(enemy);
            return;
        }

        enemiesToAttack = Physics2D.OverlapCircleAll(transform.position, range, whatIsEnemies);

        if (enemiesToAttack.Length > 0)
        {
            Soldier soldier = enemiesToAttack[0].GetComponent<Soldier>();
            if (soldier != null) AttackEnemy(enemiesToAttack[0]);            
        }
    }

    private void AttackEnemy(Collider2D enemy)
    {
        if (enemy == null) return;

        int rotationY = 0;
        if (transform.position.x > enemy.transform.position.x) rotationY = 180;
        transform.rotation = new Quaternion(0, rotationY, 0, 0);

        animator.SetTrigger("Attack");
        soldierToAttack = enemy;
    }

    protected override void AttackEnded()
    {
        if (soldierToAttack == null) return;
        projectileShooter.ShootProjectile(soldierToAttack.transform, attackDamage, armorPenetration);
    }
}
