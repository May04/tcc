﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : EnemyBase
{
    [Header("Boss")]
    [SerializeField]
    private AnimationStartedEvent animationStarted = null;
    [SerializeField]
    private float summonDelay = 0;
    private float summonTimer = 0;
    private SpawnEnemyBossPower[] spawnPoints = null;
    private int lastIndex = 0;
    private bool firstSummon = true;
    [SerializeField]
    private AudioClip summonSound = null;
    private AudioSource audioSource = null;

    private void Start()
    {
        base.Start();
        audioSource = GetComponent<AudioSource>();
        animationStarted.OnAnimationStartedAction += Summon;
        summonTimer = summonDelay;

        spawnPoints = FindObjectsOfType<SpawnEnemyBossPower>();
    }

    private void FixedUpdate()
    {
        base.FixedUpdate();
        summonTimer -= Time.deltaTime;

        if (IsBeingAttackedBySoldier) return;

        if(summonTimer <= 0)
        {
            animator.SetTrigger("Summon");
            summonTimer = summonDelay;
        }
    }

    private void Summon()
    {
        Debug.Log("summon");
        if (audioSource != null) audioSource.PlayOneShot(summonSound);

        if (spawnPoints.Length <= 0) return;

        int index = Random.Range(0, spawnPoints.Length);

        if (firstSummon == false && lastIndex == index) { Summon(); return; }

        firstSummon = false;

        spawnPoints[index].Summon();
    }

    public override void HitKill()
    {
        TakeDamage(10, 10);
    }
}
