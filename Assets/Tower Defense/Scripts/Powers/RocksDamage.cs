﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocksDamage : MonoBehaviour
{
    [SerializeField]
    private AttackEndedEvent attackEnded = null;
    [SerializeField]
    private float damage = 0;
    private Collider2D[] enemiesToAttack = null;
    [SerializeField]
    private LayerMask whatIsEnemies = new LayerMask();
    private Evaluation evaluation = null;

    private void Start()
    {
        evaluation = FindObjectOfType<Evaluation>();
        attackEnded.OnAttackEndedAction += DoDamage;
    }

    private void DoDamage()
    {
        enemiesToAttack = Physics2D.OverlapCircleAll(transform.position, 1, whatIsEnemies);

        int hitEnemies = 0;

        foreach(Collider2D enemy in enemiesToAttack)
        {
            EnemyBase enemyBase = enemy.GetComponent<EnemyBase>();
            if (enemyBase != null)
            {
                enemyBase.TakeDamage(damage, 100);
                hitEnemies++;
            }
        }

        if(evaluation != null) evaluation.OnPowerUse(PowersBase.PowerType.Rocks, hitEnemies);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, .8f);
    }
}
