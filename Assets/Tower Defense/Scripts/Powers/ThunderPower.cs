﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThunderPower : PowersBase
{
    [SerializeField]
    private AttackEndedEvent attackEnded = null;
    [SerializeField]
    private AnimationEndedEvent animationEnded = null;
    [SerializeField]
    private GameObject thunderPrefab = null;
    private EnemyBase enemyBase = null;
    private PowerGhost powerGhost = null;

    private void Start()
    {
        base.Start();
        attackEnded.OnAttackEndedAction += DoDamage;
        animationEnded.OnAnimationEndedAction += DeactivateThunder;
        powerGhost = ghostPrefab.GetComponent<PowerGhost>();
    }

    private void Update()
    {
        if (!activated) return;

        if (Input.GetMouseButtonDown(1))
        {
            ClickControllerActivator.DeactivateClickController = activated = false;
            ghostPrefab.SetActive(false);
        }

        if (Input.GetMouseButtonDown(0))
        {
            ghostPrefab.SetActive(false);

            ClickControllerActivator.DeactivateClickController = activated = false;

            enemyBase = powerGhost.Collision.GetComponent<EnemyBase>();

            if (enemyBase != null) 
            {
                Attack();
                if (!usable) StartCoroutine(AnimateFill());
            }
            
        }
    }

    protected override void Attack()
    {
        base.Attack();

        usable = false;
        thunderPrefab.transform.position = enemyBase.transform.position; 
        thunderPrefab.SetActive(true);
    }

    private void DoDamage()
    {
        if (enemyBase != null)
        {
            enemyBase.HitKill();
        }
    }

    public void DeactivateThunder()
    {
        thunderPrefab.SetActive(false);
    }
}
