﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IcePool : MonoBehaviour
{
    private Evaluation evaluation = null;
    private List<EnemyBase> enemiesHit = new List<EnemyBase>();

    private void Start()
    {
        evaluation = FindObjectOfType<Evaluation>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Enemy")
        {
            EnemyBase enemy = collision.GetComponent<EnemyBase>();

            if (enemy != null)
            {
                enemy.Freeze(true);
                if (!enemiesHit.Contains(enemy)) enemiesHit.Add(enemy);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Enemy")
        {
            EnemyBase enemy = collision.GetComponent<EnemyBase>();
            if (enemy != null) enemy.Freeze(false);
        }
    }

    private void OnDisable()
    {
        if (evaluation != null) evaluation.OnPowerUse(PowersBase.PowerType.Freeze, enemiesHit.Count);
        enemiesHit.Clear();
    }
}
