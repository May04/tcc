﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocksPower : PowersBase
{
    [SerializeField]
    private AnimationEndedEvent animationEnded = null;
    [SerializeField]
    private GameObject rocksPrefab = null;
    private Vector3 mousePosition = new Vector3();

    private void Start()
    {
        base.Start();
        animationEnded.OnAnimationEndedAction += DeactivateRocks;
    }

    protected override void Attack()
    {
        base.Attack();
        mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        rocksPrefab.transform.position = new Vector3(mousePosition.x, mousePosition.y, 0);
        rocksPrefab.SetActive(true);
        usable = false;
    }

    public void DeactivateRocks()
    {
        rocksPrefab.SetActive(false);
    }
}
