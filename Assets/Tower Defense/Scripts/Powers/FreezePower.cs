﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreezePower : PowersBase
{
    [SerializeField]
    private AnimationEndedEvent animationEnded = null;
    [SerializeField]
    private GameObject freezePrefab = null;
    private Vector3 mousePosition = new Vector3();

    private void Start()
    {
        base.Start();
        animationEnded.OnAnimationEndedAction += DeactivateFreeze;
    }

    protected override void Attack()
    {
        base.Attack();
        mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        freezePrefab.transform.position = new Vector3(mousePosition.x, mousePosition.y, 0);
        freezePrefab.SetActive(true);
        usable = false;
    }

    public void DeactivateFreeze()
    {
        freezePrefab.SetActive(false);
    }
}
