﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerGhost : MonoBehaviour
{
    private Vector3 mousePosition = new Vector3();
    private Collider2D collision = null;

    public Collider2D Collision => collision;

    void Update()
    {
        mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = new Vector3(mousePosition.x, mousePosition.y, 0); 
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        this.collision = collision;

        Debug.Log(collision);
    }
}
