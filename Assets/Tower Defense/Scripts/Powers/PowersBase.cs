﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowersBase : MonoBehaviour
{
    protected bool activated = false;
    protected RaycastHit2D[] hit = null;
    protected RaycastHit2D targetHit = new RaycastHit2D();
    [SerializeField]
    protected GameObject ghostPrefab = null;
    [SerializeField]
    private float delay = 0;
    [SerializeField]
    private Image timer = null;
    protected bool usable = true;

    protected void Start()
    {
        timer.material.SetFloat("_Arc1", 360);
    }

    public void OnClick()
    {
        if (usable)
        {
            ClickControllerActivator.DeactivateClickController = activated = true;
            ghostPrefab.SetActive(true);
        }
    }

    private void Update()
    {
        if (!activated) return;

        if (Input.GetMouseButtonDown(1))
        {
            ClickControllerActivator.DeactivateClickController = activated = false;
            ghostPrefab.SetActive(false);
        }

            if (Input.GetMouseButtonDown(0))
        {
            ghostPrefab.SetActive(false);

            ClickControllerActivator.DeactivateClickController = activated = false;

            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);
            hit = Physics2D.RaycastAll(mousePos2D, Vector2.zero);

            foreach(RaycastHit2D h in hit)
            {
                if (h.transform.tag == "Enemy")
                {
                    targetHit = h;
                    Attack();
                    if (!usable) StartCoroutine(AnimateFill());
                    break;
                }
                else if (h.transform.tag == "Path")
                {
                    targetHit = h;
                    Attack();
                    if (!usable) StartCoroutine(AnimateFill());
                    break;
                }
            }
        }
    }

    protected virtual void Attack() { }

    protected IEnumerator AnimateFill()
    {
        if (delay > 0)
        {       
            timer.material.SetFloat("_Arc1", 0);
            float time = 0;
            float startValue = 0;
            float endValue = 360;

            while (time <= delay)
            {
                timer.material.SetFloat("_Arc1", Mathf.Lerp(startValue, endValue, time / delay));
                time += Time.deltaTime;
                yield return null;
            }

            usable = true;
        }
    }

    public enum PowerType
    {
        Thunder,
        Rocks,
        Freeze
    }
}
