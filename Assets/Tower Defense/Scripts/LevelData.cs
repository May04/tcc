﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new Level Data", menuName = "Tower Defense/Level Data")]
public class LevelData : ScriptableObject
{
    [SerializeField]
    private bool unlocked = false;
    [SerializeField]
    private int rating = 0;
    private GameManager gameManager = null;

    public System.Action OnValuesChanged;

    public bool Unlocked  
    {
        get => unlocked;
        set
        {
            if (unlocked == false) unlocked = value;
        }
    }

    public int Rating { get => rating; set => rating = value; }

    public void SaveValues()
    {
        if (gameManager == null) gameManager = FindObjectOfType<GameManager>();

        if(gameManager != null) gameManager.SaveLevelData();
    }
}
