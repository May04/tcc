﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationStartedEvent : MonoBehaviour
{
    public System.Action OnAnimationStartedAction;

    public void OnAnimationStarted()
    {
        OnAnimationStartedAction?.Invoke();
    }
}
