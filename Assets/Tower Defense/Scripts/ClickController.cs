﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickController : MonoBehaviour
{
    private Clickable clickable
    {
        get
        {
            if (lastHit == null) return null;
            if (lastHit.transform == null) return null;
            return lastHit.transform.GetComponent<Clickable>();
        }
    }

    private Collider2D lastHit = null;
    private RaycastHit2D hit;

    protected void Update()
    {
        if (ClickControllerActivator.DeactivateClickController) return;

        VerifyHover();

        if (Input.GetMouseButtonDown(0)) if(clickable != null) clickable.OnClickVerify();

        if (Input.GetMouseButtonUp(0)) if (clickable != null) clickable.OnClickUpVerify();
    }

    protected void VerifyHover()
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);
        hit = Physics2D.Raycast(mousePos2D, Vector2.zero);


        if (hit.collider != null)
        {
            if (hit.collider != lastHit && clickable != null) clickable.OnHoverExitVerify();

            lastHit = hit.collider;

            if (clickable != null)
            {
                clickable.OnHoverVerify();
            }
        }
        else
        {
            if (clickable != null) clickable.OnHoverExitVerify();
            lastHit = null;
        }
    }
}

public static class ClickControllerActivator
{
    public static bool DeactivateClickController = false;
} 
