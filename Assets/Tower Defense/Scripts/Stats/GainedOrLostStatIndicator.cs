﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GainedOrLostStatIndicator: MonoBehaviour
{
    [SerializeField]
    private TextMeshPro text = null;
    
    public int ValuePlusText { set => text.text = "+" + value; }
    public int ValueMinusText { set => text.text = "-" + value; }

    [HideInInspector]
    public bool DestroyOnDisable = false;

    private void OnDisable()
    {
        if (DestroyOnDisable) Destroy(this.gameObject);
    }
}
