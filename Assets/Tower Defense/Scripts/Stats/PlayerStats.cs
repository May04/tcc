﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new Player Stats", menuName = "Tower Defense/Player Stats")]
public class PlayerStats : ScriptableObject
{
    private int diamonds = 0;
    private int hearts = 0;
    private int waves = 0;
    private int totalWaves;

    public System.Action<int> OnDiamondsChange;
    public System.Action<int> OnHeartsChange;
    public System.Action<int> OnWavesChange;

    public int Diamonds { get => diamonds; set => diamonds = value; }
    public int Hearts { get => hearts; set => hearts = value; }

    public int Waves { get => waves; set => waves = value; }
    public int TotalWaves { get => totalWaves; set => totalWaves = value; }

    public void IncreaseDiamondsIn(int value = 1)
    {
        diamonds += value;
        OnDiamondsChange?.Invoke(diamonds);        
    }

    public void DecreaseDiamondsIn(int value = 1)
    {
        if (diamonds == 0) return;

        diamonds -= value;
        OnDiamondsChange?.Invoke(diamonds);
    }

    public void IncreaseHeartsIn(int value = 1)
    {
        if (hearts == 0) return;

        hearts += value;
        OnHeartsChange?.Invoke(hearts);
    }

    public void DecreaseHeartsIn(int value = 1)
    {
        if (hearts == 0) return;

        hearts -= value;
        OnHeartsChange?.Invoke(hearts);
    }

    public void AdvanceWave(int value)
    {
        if (waves >= totalWaves) return;
        waves = value;
        OnWavesChange?.Invoke(waves);
    }
}
