﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HUDManager : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI diamondsText = null;
    [SerializeField]
    private TextMeshProUGUI heartsText = null;
    [SerializeField]
    private Image heartsFill = null;
    [SerializeField]
    private TextMeshProUGUI wavesText = null;
    [SerializeField]
    private PlayerStats stats = null;
    [SerializeField]
    private GameObject pauseScreen = null;
    private Evaluation evaluation = null;

    private int totalHearts = 0;

    private void Start()
    {
        evaluation = FindObjectOfType<Evaluation>();

        DiamondsValue(stats.Diamonds);
        HeartsValueText(stats.Hearts);
        totalHearts = stats.Hearts;
        HeartsValueBar(stats.Hearts);
        WavesValueText(0);

        stats.OnDiamondsChange += DiamondsValue;
        stats.OnHeartsChange += HeartsValueBar;
        stats.OnHeartsChange += HeartsValueText;
        stats.OnWavesChange += WavesValueText;
    }

    public void DiamondsValue(int value) 
    {
        diamondsText.text = value.ToString();     
    }

    public void HeartsValueText(int value)
    {
        heartsText.text = value.ToString();  
    }

    public void HeartsValueBar(int value)
    {
        if (totalHearts == 0) return;

        heartsFill.fillAmount = ((100f * value / totalHearts) / 100f);
    }

    public void WavesValueText(int value)
    {
        wavesText.text = value.ToString() + "/" + stats.TotalWaves.ToString();
        if(evaluation != null) evaluation.OnNewWave(value);
    }

    private void OnDestroy()
    {
        stats.OnDiamondsChange -= DiamondsValue;
        stats.OnHeartsChange -= HeartsValueBar;
        stats.OnHeartsChange -= HeartsValueText;
        stats.OnWavesChange -= WavesValueText;
    }

    public void PauseScreen() 
    {
        pauseScreen.SetActive(!pauseScreen.activeInHierarchy);
    }
}
