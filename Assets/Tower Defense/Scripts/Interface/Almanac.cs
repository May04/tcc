﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Almanac : MonoBehaviour
{
    [SerializeField]
    private GameObject enemies = null;
    [SerializeField]
    private GameObject towersAndPowers = null;
    [SerializeField]
    private Text changePageButtonText = null;

    private void OnEnable()
    {
        ChangeState();
        Time.timeScale = 0;
    }

    private void OnDisable()
    {
        Time.timeScale = 1;
    }

    private void ChangeState()
    {
        if (enemies.activeInHierarchy)
        {
            changePageButtonText.text = "Ir para Torres e Poderes";
            towersAndPowers.SetActive(false);
        }
        else
        {
            changePageButtonText.text = "Ir para Inimigos";
            towersAndPowers.SetActive(true);
        }
    }

    public void ChangePage()
    {
        enemies.SetActive(!enemies.activeInHierarchy);
        towersAndPowers.SetActive(!enemies.activeInHierarchy);
        ChangeState();
    }

    public void CloseAlmanac()
    {
        gameObject.SetActive(false);
    }
}
