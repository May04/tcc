﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class NoMoneyUI : MonoBehaviour
{
    [SerializeField]
    private float lifetime = 0;
    [SerializeField]
    private TextMeshProUGUI text = null;
    private void Start()
    {
        text.enabled = false;
    }

    public void Enable()
    {
        transform.position = Input.mousePosition;
        StartCoroutine(Blink());
    }
    
    private IEnumerator Blink()
    {
        text.enabled = true; 
        yield return new WaitForSeconds(lifetime);
        text.enabled = false;
    }
}
