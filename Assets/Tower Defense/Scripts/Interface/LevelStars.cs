﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelStars : MonoBehaviour
{
    [SerializeField]
    private Sprite noStars = null;
    [SerializeField]
    private Sprite oneStar = null;
    [SerializeField]
    private Sprite twoStars = null;
    [SerializeField]
    private Sprite threeStars = null;
}
