﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndLevelWindow : MonoBehaviour
{
    [SerializeField]
    private Image starsImage = null;
    [SerializeField]
    private Text description = null;
    [SerializeField]
    private Image header = null;
    [SerializeField]
    private Sprite noStarSprite = null;
    [SerializeField]
    private Sprite oneStarSprite = null;
    [SerializeField]
    private Sprite twoStarSprite = null;
    [SerializeField]
    private Sprite threeStarSprite = null;
    [SerializeField]
    private Sprite victoryHeader = null;
    [SerializeField]
    private Sprite loseHeader = null;
    [SerializeField]
    private GameObject tips = null;
    [SerializeField]
    private GameObject scrollView = null;
    [SerializeField]
    private GameObject tipPrefab = null;
    [SerializeField]
    private AudioClip winSound = null;
    [SerializeField]
    private AudioClip loseSound = null;
    
    private void OnEnable() 
    {
        scrollView.SetActive(tips.transform.childCount > 0);
    }

    public void SetNoStar()
    {
        starsImage.sprite = noStarSprite;
        header.sprite = loseHeader;
    }

    public void SetOneStar()
    {
        starsImage.sprite = oneStarSprite;
        header.sprite = victoryHeader;
    }

    public void SetTwoStar()
    {
        starsImage.sprite = twoStarSprite;
        header.sprite = victoryHeader;
    }

    public void SetThreeStar()
    {
        starsImage.sprite = threeStarSprite;
        header.sprite = victoryHeader;
    }

    public void SetDescription(string description)
    {
        this.description.text = description;
    }

    public void PlayLoseSound()
    {
        AudioSource aus = GetComponent<AudioSource>();
        if (aus != null)
        {
            aus.clip = loseSound;
            aus.Play();
        }
    }

    public void PlayWinSound()
    {
        AudioSource aus = GetComponent<AudioSource>();
        if (aus != null)
        {
            aus.clip = winSound;
            aus.Play();
        }
    }

    public void AddTip(string tipText)
    {
        GameObject newTip = Instantiate(tipPrefab, tips.transform);
        newTip.GetComponentInChildren<Text>().text = tipText;
    }

    public void GoToMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    public void Retry()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}

