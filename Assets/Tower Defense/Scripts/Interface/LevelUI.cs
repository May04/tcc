﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelUI : MonoBehaviour
{
    [SerializeField]
    private LevelData data = null;
    [SerializeField]
    private GameObject description = null;
    [SerializeField]
    private GameObject stars = null;
    [SerializeField]
    private GameObject playButton = null;
    [SerializeField]
    private Sprite noStars = null;
    [SerializeField]
    private Sprite oneStars = null;
    [SerializeField]
    private Sprite twoStars = null;
    [SerializeField]
    private Sprite threeStars = null;
    [SerializeField]
    private GameObject locker = null;

    private bool unlocked = false;
    private int rating = 0;

    private void Awake()
    {
        data.OnValuesChanged += OnDataLoad;        
    }

    private void OnDataLoad()
    {
        unlocked = data.Unlocked;
        rating = data.Rating;

        description.SetActive(unlocked);
        playButton.SetActive(unlocked);
        stars.SetActive(unlocked);

        if (locker != null) locker.SetActive(!unlocked);

        Image starsImage = stars.GetComponent<Image>();

        switch (rating)
        {
            case 1:
                starsImage.sprite = oneStars;
                break;
            case 2:
                starsImage.sprite = twoStars;
                break;
            case 3:
                starsImage.sprite = threeStars;
                break;
            default:
                starsImage.sprite = noStars;
                break;
        }
    }

    private void OnDestroy()
    {
        data.OnValuesChanged -= OnDataLoad;
    }
}
