﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TowerTypeSelector : Clickable
{
    [SerializeField]
    private TowerType towerType = null;
    [SerializeField]
    private TowerSelector towerSelector = null;
    [SerializeField]
    private TextMeshPro priceText = null;
    [SerializeField]
    private Transform soldierSpot = null;

    private GameObject ghostTower = null;

    private void OnEnable()
    {
        GetComponent<SpriteRenderer>().sprite = towerType.Icon;
        transform.localScale = towerType.IconScale;
        priceText.text = towerType.BuyPrice.ToString();
    }

    public override void OnClick()
    {
        base.OnClick();

        if (towerSelector.Stats.Diamonds < towerType.BuyPrice)
        {
            FindObjectOfType<NoMoneyUI>().Enable();
            return;
        }

        towerSelector.TowerSelected.Invoke(towerType);

        if (ghostTower == null) return;

        Destroy(ghostTower);
    }

    public override void OnHover()
    {
        base.OnHover();
        if (ghostTower != null) Destroy(ghostTower);

        ghostTower = Instantiate(towerType.GhostPrefab, transform.parent.parent.parent);

        SpriteRenderer[] sprites = ghostTower.GetComponentsInChildren<SpriteRenderer>();
        
        foreach(SpriteRenderer sprite in sprites)
        {
            sprite.sortingLayerName = towerSelector.towerSortingLayer.ToString();
        }

        if (towerType.TypeOfTower == TowerType.Type.Soldier) ghostTower.GetComponent<SoldierGhost>().SoldierSpot = soldierSpot;
    }

    public override void OnHoverExit()
    {
        base.OnHoverExit();
        if (ghostTower == null) return;

        Destroy(ghostTower);
    }
}
