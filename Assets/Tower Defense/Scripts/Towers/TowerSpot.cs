﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

[RequireComponent(typeof(CapsuleCollider2D))]
public class TowerSpot : Clickable
{
    [SerializeField]
    private TowerSelector towerSelection = null;
    private bool hasTower = false;
    private SpriteRenderer spriteRenderer = null;

    [Header("Evaluation")]
    private Evaluation evaluation = null;
    [SerializeField]
    private TowerRecomendation soldierTowerRating = new TowerRecomendation();
    [SerializeField]
    private TowerRecomendation archerTowerRating = new TowerRecomendation();
    [SerializeField]
    private TowerRecomendation mageTowerRating = new TowerRecomendation();
    [SerializeField]
    private TowerRecomendation bomberTowerRating = new TowerRecomendation();
    [SerializeField]
    private TowerSpot[] surroundingTowerSpots = null;

    private int positioningScore = 0;
    private int meleeSurrondingCount = 0;
    private bool isMelee = false;

    public bool HasTower => hasTower;
    public bool IsMelee => isMelee;
    public int PositioningScore => positioningScore;
    public int MeleeSurrondingCount
    {
        get
        {
            meleeSurrondingCount = 0;
            foreach(TowerSpot spot in surroundingTowerSpots)
            {
                if (spot.HasTower && !spot.IsMelee) meleeSurrondingCount++;
            }

            return meleeSurrondingCount;
        }
    }

    private void Reset()
    {
        CapsuleCollider2D capsuleCollider = GetComponent<CapsuleCollider2D>();
        capsuleCollider.direction = CapsuleDirection2D.Horizontal;
        capsuleCollider.isTrigger = true;
    }

    private void Start()
    {
        evaluation = FindObjectOfType<Evaluation>();

        spriteRenderer = GetComponent<SpriteRenderer>();
        towerSelection.TowerSelected += TowerSelected;
        towerSelection.TowerSold += TowerSold;
        towerSelection.BuyTowerMenu(false);
    }

    public override void OnClick()
    {
        base.OnClick();

        if (hasTower) return;

        towerSelection.BuyTowerMenu(true);
        mouseOnMe = true;
    }

    public override void OnHover()
    {
        base.OnHover();
        if (spriteRenderer == null) return;
        spriteRenderer.color = new Color(0.745283f, 0.745283f, 0.745283f);
    }

    public override void OnHoverExit()
    {
        base.OnHoverExit();
        if (spriteRenderer == null) return;
        spriteRenderer.color = new Color(1f, 1f, 1f);
    }

    private void LateUpdate()
    {
        if (hasTower) return;

        if (Input.GetMouseButtonDown(0))
        {
            if (!mouseOnMe && towerSelection.gameObject.activeInHierarchy)
            {
                towerSelection.BuyTowerMenu(false);
            }
        }

        mouseOnMe = false;
    }

    private void TowerSelected(TowerType towerType)
    {
        hasTower = true;
        towerSelection.BuyTowerMenu(false);

        switch (towerType.TypeOfTower)
        {
            case TowerType.Type.Soldier:
                positioningScore = (int)soldierTowerRating;
                isMelee = true;
                break;
            case TowerType.Type.Archer:
                positioningScore = (int)archerTowerRating;
                break;
            case TowerType.Type.Mage:
                positioningScore = (int)mageTowerRating;
                break;
            case TowerType.Type.Bomber:
                positioningScore = (int)bomberTowerRating;
                break;
            default:
                positioningScore = 0;
                break;
        }

        if(evaluation != null) evaluation.OnAddTower(towerType, this);
    }

    private void TowerSold()
    {
        positioningScore = 0;
        hasTower = false;
        isMelee = false;
        if (evaluation != null) evaluation.OnSellTower(this);
    }

    private enum TowerRecomendation
    {
        recommended = 2,
        sufficient = 1,
        notRecommended = 0
    }
}
