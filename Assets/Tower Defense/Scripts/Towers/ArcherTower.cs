﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcherTower : RangedTowerBase
{
    [Header("Archer")]
    [SerializeField]
    private Transform archerSpriteTransform = null;
   
    protected override void Attack()
    {
        base.Attack();

        int rotationY = 0;
        if (archerSpriteTransform.position.x > enemyToAttack.transform.position.x) rotationY = 180;
        archerSpriteTransform.rotation = new Quaternion(0, rotationY, 0, 0);
    }
}
