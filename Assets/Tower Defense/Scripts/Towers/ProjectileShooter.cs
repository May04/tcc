﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileShooter : MonoBehaviour
{
    [SerializeField]
    private GameObject projectilePrefab = null;
    private AudioSource audioSource = null;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void ShootProjectile(Transform target, float damage, float armorPenetration) 
    {
        if (target == null) return;

        Projectile projectile = Instantiate(projectilePrefab, transform).GetComponent<Projectile>();

        if (audioSource != null) audioSource.Play();

        projectile.Target = target;
        projectile.SpawnPosition = transform;
        projectile.Damage = damage;
        projectile.ArmorPenetration = armorPenetration;
    }
}
