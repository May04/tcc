﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Soldier : Characters, ITakeDamage
{
    [SerializeField]
    private float range = 0;
    [SerializeField]
    private float moveSpeed = 0;
    [SerializeField]
    private Animator animator = null;
    [SerializeField]
    private LayerMask whatIsEnemies = new LayerMask();
    [SerializeField]
    private AttackEndedEvent attackEnded = null;
    [SerializeField]
    private float life = 5;
    [SerializeField]
    private float armor = 5;
    [SerializeField]
    private float attackDamage = 0;
    [SerializeField]
    private float armorPenetration = 0;
    [SerializeField]
    private float attackSpeed = 2;
    private bool move = false;
    private bool reachedTarget = false;
    private Transform moveTarget = null;
    private float attackSpeedTimer = 0;
    private float moveDifference = 0;
    private EnemyBase enemyToAttack = null;
    private float currentLife = 0;
    private AudioSource audioSource = null;
    [SerializeField]
    private AudioClip[] audioClips = null;

    public float AttackDamage { set => attackDamage = value; }
    public float ArmorPenetration { set => armorPenetration = value; }
    public float AttackSpeed { set => attackSpeed = value; }

    public System.Action OnDie;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        attackEnded.OnAttackEndedAction += AttackEnded;
        attackSpeedTimer = attackSpeed;
        maxLife = currentLife = life;
    }

    private void Update()
    {
        if (enemyToAttack == null)
        {
            reachedTarget = false;
            Move(transform.parent, 0);
            FindEnemy();
        }

        if(move && moveTarget != null)
        {
            if (Vector2.Distance(moveTarget.transform.position, transform.parent.position) > 20 * range)
            {
                if (enemyToAttack != null)
                {
                    enemyToAttack.IsBeingAttackedBySoldier = false;
                    enemyToAttack.AttackingSoldier = null;
                    enemyToAttack = null;
                }
                Move(transform.parent, 0);
                return;
            }

            if (enemyToAttack != null) enemyToAttack.IsBeingAttackedBySoldier = true;

            if (Vector2.Distance(moveTarget.position, transform.position) <= moveDifference)
            {
                if (enemyToAttack != null) enemyToAttack.AttackingSoldier = this;
                move = false;
                reachedTarget = true;
            }

            if(move)
            {
                animator.SetBool("Walk", true);
                transform.position = Vector2.MoveTowards(transform.position, moveTarget.position, moveSpeed);
            }
            else
            {
                animator.SetBool("Walk", false);
            }

        }

        attackSpeedTimer -= Time.deltaTime;

        if (attackSpeedTimer <= 0 && enemyToAttack != null && reachedTarget) Attack();
    }

    private void FindEnemy()
    {
        Collider2D[] enemiesToAttack = Physics2D.OverlapCircleAll(transform.position, range, whatIsEnemies);

        if (enemiesToAttack.Length > 0)
        {
            foreach(Collider2D enemyCol in enemiesToAttack)
            {
                EnemyBase enemy = enemyCol.GetComponent<EnemyBase>();

                if (enemy.NonTargetablebySoldier || enemy.IsBeingAttackedBySoldier)
                {
                    Move(transform.parent, 0);
                    return;
                }                

                Move(enemyCol.transform, 0.2f);
                enemyToAttack = enemy;
            }
        }
    }

    private void Attack()
    {
        attackSpeedTimer = attackSpeed;
        animator.SetTrigger("Attack");
        if (audioSource != null)
        {
            int rand = Random.Range(0, audioClips.Length - 1);
            audioSource.PlayOneShot(audioClips[rand]);
        }
    }

    private void AttackEnded()
    {
        attackSpeedTimer = attackSpeed;
        if (enemyToAttack == null) return;
        enemyToAttack.TakeDamage(attackDamage, armorPenetration, true);
    }

    public void Move(Transform target, float difference)
    {
        int rotationY = 0;
        if (transform.position.x > target.position.x) rotationY = 180;
         transform.rotation = new Quaternion(0, rotationY, 0, 0);

        moveTarget = target;
        moveDifference = difference;
        move = true;
    }

    public void TakeDamage(float damage, float armorPenetration, bool tower = false)
    {
        float currentArmor = armorPenetration > armor ? 0 : armor - armorPenetration;

        float deflectedDamage = (damage * currentArmor) / 100;

        float trueDamage = damage - deflectedDamage;

        currentLife -= trueDamage;

        SetLife(currentLife);

        if (currentLife <= 0) Die();

        StartCoroutine(DamageBlink());
    }

    private IEnumerator DamageBlink()
    {
        int i = 0;
        while (i < 3)
        {
            spriteRenderer.color = Color.red;
            yield return new WaitForSeconds(.2f);
            spriteRenderer.color = Color.white;
            yield return new WaitForSeconds(.2f);
            i++;
        }
    }

    private void Die()
    {
        OnDie?.Invoke();

        if(enemyToAttack != null)
        {
            enemyToAttack.IsBeingAttackedBySoldier = false;
            enemyToAttack.AttackingSoldier = null;
        }

        Destroy(this.gameObject);
    }

    private void OnDestroy()
    {
        if (enemyToAttack != null)
        {
            enemyToAttack.IsBeingAttackedBySoldier = false;
            enemyToAttack.AttackingSoldier = null;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }
}
