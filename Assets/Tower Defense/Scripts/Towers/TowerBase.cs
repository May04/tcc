﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider2D))]
public class TowerBase : Clickable
{
    [Header("Stats")]
    [SerializeField]
    protected float attackDamage = 0;
    [SerializeField]
    protected float range = 0;
    [SerializeField]
    protected float attackSpeed = 0;
    [SerializeField]
    protected float armorPenetration = 0;

    [Header("Components")]
    [SerializeField]
    protected Animator animator = null;
    [SerializeField]
    protected AttackEndedEvent towerAttackEndedEvent = null;
    [SerializeField]
    protected GameObject rangeIndicator = null;

    [Header("Attack")]
    [SerializeField]
    protected LayerMask whatIsEnemies = new LayerMask();

    protected Collider2D[] enemiesToAttack;
    protected EnemyBase enemyToAttack;

    protected float attackSpeedTimer = 0;
    protected bool attackEnded = true;

    public System.Action<bool> OnClickOnTower;

    protected void Start()
    {
        if(towerAttackEndedEvent != null) towerAttackEndedEvent.OnAttackEndedAction += OnAttackEnd;
        if(rangeIndicator != null) rangeIndicator.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0)) if (!mouseOnMe) OnClickOnTower?.Invoke(false);
        mouseOnMe = false;

        if (rangeIndicator == null) return;

        if (!attackEnded) return;

        attackSpeedTimer -= Time.deltaTime;

        enemiesToAttack = Physics2D.OverlapCircleAll(rangeIndicator.transform.position, range, whatIsEnemies);

        if (enemiesToAttack.Length <= 0) return;

        EnemyBase enemyCloserToEnd = null;

        foreach(Collider2D enemy in enemiesToAttack)
        {
            EnemyBase enemyBase = enemy.GetComponent<EnemyBase>();

            if (enemyCloserToEnd == null) enemyCloserToEnd = enemyBase;
            if (enemyBase.DistanceToEnd < enemyCloserToEnd.DistanceToEnd) enemyCloserToEnd = enemyBase;
        }

        enemyToAttack = enemyCloserToEnd;

        if (enemyToAttack.NonTargetable) return;

        if (attackSpeedTimer <= 0) Attack();
    }

    protected virtual void Attack() 
    {
        attackEnded = false;
        attackSpeedTimer = attackSpeed;
        if(animator != null) animator.Play("Attack");
    }

    protected virtual void OnAttackEnd() 
    {
        attackEnded = true;
    }

    public override void OnHover()
    {
        base.OnHover();
        if (rangeIndicator != null) rangeIndicator.gameObject.SetActive(true);
    }

    public override void OnHoverExit()
    {
        base.OnHoverExit();
        if (rangeIndicator != null) rangeIndicator.gameObject.SetActive(false);
    }

    public override void OnClick()
    {
        base.OnClick();
        OnClickOnTower?.Invoke(true);
        mouseOnMe = true;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        if (rangeIndicator != null) Gizmos.DrawWireSphere(rangeIndicator.transform.position, range);
    }
}
