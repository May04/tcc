﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;

public class SellTowerSelector : Clickable
{
    [SerializeField]
    private TextMeshPro sellPriceText = null;
    [SerializeField]
    private GameObject highlight = null;

    public System.Action TowerSold;

    public string SellPrice
    {
        set
        {
            sellPriceText.text = value;
        }
    }

    public override void OnClick()
    {
        base.OnClick();

        TowerSold?.Invoke();
    }

    public override void OnHover()
    {
        base.OnHover();
        highlight.SetActive(true);
    }

    public override void OnHoverExit()
    {
        base.OnHoverExit();
        highlight.SetActive(false);
    }

    private void OnDisable()
    {
        transform.DOComplete();
    }
}
