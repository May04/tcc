﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedTowerBase : TowerBase
{
    [SerializeField]
    private ProjectileShooter projectileShooter = null;

    protected override void OnAttackEnd()
    {
        base.OnAttackEnd();
        if (enemyToAttack == null) return;
        projectileShooter.ShootProjectile(enemyToAttack.transform, attackDamage, armorPenetration);
    }
}
