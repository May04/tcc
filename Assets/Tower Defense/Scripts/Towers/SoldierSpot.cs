﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoldierSpot : MonoBehaviour
{
    public Soldier soldier = null;

    public void MoveSoldier()
    {
        soldier.Move(transform, 0);
    }
}
