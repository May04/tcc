﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class TowerSelector : MonoBehaviour
{
    [SerializeField]
    private float selectorInterfaceScale = 1.5f;
    [SerializeField]
    private GameObject buyTowerMenu = null;
    [SerializeField]
    private GameObject sellTowerMenu = null;
    [SerializeField]
    private SellTowerSelector sellTowerSelector = null;
    [SerializeField]
    private PlayerStats stats = null;
    [SerializeField]
    private Transform soldierDefaultPoint = null;
    public TowerSortingLayer towerSortingLayer = new TowerSortingLayer();

    private CurrentTower currentTower = new CurrentTower();

    public Action<TowerType> TowerSelected = null;
    public Action TowerSold = null;
    public PlayerStats Stats => stats;

    private void Awake()
    {
        TowerSelected += AddTower;
        sellTowerSelector.TowerSold += SellTower;
    }

    public void BuyTowerMenu(bool active)
    {
        buyTowerMenu.transform.localScale = Vector3.zero;
        buyTowerMenu.transform.DOScale(selectorInterfaceScale, 1);
        buyTowerMenu.SetActive(active);
        if(!active) buyTowerMenu.transform.DOComplete();
    }

    public void AddTower(TowerType towerType)
    {
        if (currentTower.GameObject != null) return;

       

        currentTower.TowerType = towerType;
        currentTower.GameObject = Instantiate(towerType.Prefab, transform.parent);
        sellTowerSelector.SellPrice = towerType.SellPrice.ToString();

        if (towerType.TypeOfTower == TowerType.Type.Soldier)
        {
            SoldierTower st = currentTower.GameObject.GetComponent<SoldierTower>();
            st.DefaultSoldierPoint = soldierDefaultPoint;           
        }

        SpriteRenderer[] towerSprites = currentTower.GameObject.GetComponentsInChildren<SpriteRenderer>();

        foreach (SpriteRenderer sprite in towerSprites) sprite.sortingLayerName = towerSortingLayer.ToString();

        currentTower.GameObject.GetComponent<TowerBase>().OnClickOnTower += ActivateTowerSell;

        stats.DecreaseDiamondsIn(towerType.BuyPrice);
    }

    public void SellTower()
    {
        if (currentTower.GameObject == null) return;

        sellTowerMenu.SetActive(false);
        sellTowerMenu.transform.DOComplete();

        Destroy(currentTower.GameObject);
        stats.IncreaseDiamondsIn(currentTower.TowerType.SellPrice);

        currentTower = new CurrentTower();

        TowerSold?.Invoke();
    }

    private void ActivateTowerSell(bool active)
    {
        sellTowerMenu.transform.localScale = Vector3.zero;
        sellTowerMenu.transform.DOScale(selectorInterfaceScale, 1);
        sellTowerMenu.SetActive(active);
        if(!active) sellTowerMenu.transform.DOComplete();
    }

    private struct CurrentTower
    {
        public GameObject GameObject;
        public TowerType TowerType;
    }

    public enum TowerSortingLayer
    {
        Towers,
        FrontTowers
    }
}
