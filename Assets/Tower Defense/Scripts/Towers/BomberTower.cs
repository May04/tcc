﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BomberTower : TowerBase
{
    [SerializeField]
    private GameObject bomb = null;

    protected override void OnAttackEnd()
    {
        base.OnAttackEnd();

        if (enemyToAttack == null)
        {
            attackSpeedTimer = 0;
            return;
        }

        GameObject newBomb = Instantiate(bomb);
        Bomb newBombSc = newBomb.GetComponent<Bomb>();
        newBombSc.Damage = attackDamage;
        newBombSc.ArmorPenetration = armorPenetration;
        newBomb.transform.position = enemyToAttack.transform.position;
    }
}
