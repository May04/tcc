﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new Tower Type", menuName = "Tower Defense/Towers/Tower Type")]
public class TowerType : ScriptableObject
{
    public Type TypeOfTower;
    public GameObject Prefab;
    public GameObject GhostPrefab;
    public Sprite Icon;
    public Vector3 IconScale;
    public int BuyPrice;
    public int SellPrice;

    [System.Serializable]
    public enum Type
    {
        Archer,
        Soldier,
        Mage,
        Bomber
    }
}
