﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class SoldierTower : TowerBase
{
    [Header("Soldier Tower")]
    [SerializeField]
    private Transform defensePoint = null;
    [HideInInspector]
    public Transform DefaultSoldierPoint = null;
    public Collider2D RangeCollider = null;
    [SerializeField]
    private float spawnDelay = 8;
    [SerializeField]
    private GameObject soldierPrefab = null;
    private SoldierSpot[] soldierSpots = null;

    public System.Action OnDefenseSpotChanged;

    private void Start()
    {
        base.Start();
        defensePoint.transform.position = DefaultSoldierPoint.position;

        soldierSpots = defensePoint.GetComponentsInChildren<SoldierSpot>();

        foreach (SoldierSpot spot in soldierSpots)
        {
            OnDefenseSpotChanged += spot.MoveSoldier;
            StartCoroutine(SpawnSoldier(0, spot));
        }
    }

    private IEnumerator SpawnSoldier(float delay, SoldierSpot spot)
    {
        yield return new WaitForSeconds(delay);
        if (spot.soldier == null)
        {
            Soldier newSoldier = Instantiate(soldierPrefab, spot.transform).GetComponent<Soldier>();
            newSoldier.transform.localPosition = Vector3.zero;
            spot.soldier = newSoldier;                
            newSoldier.OnDie += () => { StartCoroutine(SpawnSoldier(spawnDelay, spot)); };
        }        
    }
}
