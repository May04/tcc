﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField]
    private float speed = 10f;
    private Transform target = null;
    private Vector3 targetPosition = new Vector3();
    private Transform spawnPosition = null;
    private Vector3 next = new Vector3();
    private float dist = 0;
    private float damage = 0;
    private float armorPenetration = 0;
    private Vector3 lastTargetPos = new Vector3();

    public Transform Target { set { target = value; targetPosition = target.position; } }

    public Transform SpawnPosition { set => spawnPosition = value; }

    public float Damage { set => damage = value; }

    public float ArmorPenetration { set => armorPenetration = value; }

    void Update()
    {
        if(target == null)
        {
            Destroy(this.gameObject);
            return;
        }

        if (target.position == null) return;
        
        targetPosition = target.position;

        dist = Vector2.Distance(targetPosition, spawnPosition.position);
        next = Vector2.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);

        transform.rotation = LookAtTarget(next - transform.position);
        transform.position = next;
    }

    public static Quaternion LookAtTarget(Vector2 r)
    {
        return Quaternion.Euler(0, 0, Mathf.Atan2(r.y, r.x) * Mathf.Rad2Deg);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (target == null) return;
        if(collision == target.GetComponent<Collider2D>())
        {
            ITakeDamage takeDamage = collision.GetComponent<ITakeDamage>();
            if(takeDamage != null) takeDamage.TakeDamage(damage, armorPenetration, true);
            Destroy(this.gameObject);
        }
    }
}