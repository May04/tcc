﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    [SerializeField]
    private AttackEndedEvent attackEnded = null;
    [SerializeField]
    private AnimationEndedEvent animationEnded = null;
    [SerializeField]
    private LayerMask whatIsEnemies = new LayerMask();
    private float damage = 0;
    private float armorPenetration = 0;

    public float Damage { set => damage = value; }
    public float ArmorPenetration { set => armorPenetration = value; }

    private void Start()
    {
        attackEnded.OnAttackEndedAction += OnAttackEnded;
        animationEnded.OnAnimationEndedAction += OnAnimationEnded;
    }

    private void OnAttackEnded() 
    {
         Collider2D[] enemiesToAttack = Physics2D.OverlapCircleAll(transform.position, .6f, whatIsEnemies);

        foreach(Collider2D enemy in enemiesToAttack)
        {
            EnemyBase enemyBase = enemy.GetComponent<EnemyBase>();
            if (enemyBase == null) return;
            enemyBase.TakeDamage(damage, armorPenetration);
        }
    }

    private void OnAnimationEnded()
    {
        Destroy(this.gameObject);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, .6f);
    }
}
