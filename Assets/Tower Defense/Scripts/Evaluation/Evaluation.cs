﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using OfficeOpenXml;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Evaluation : MonoBehaviour
{
    private List<TowerType.Type> towerTypes = new List<TowerType.Type>();
    private int addTowerCount = 0;
    private int sellTowerCount = 0;
    private int towersCount = 0;
    private float towerPositioningScore = 0;
    private float meleeTowerSurrondingScore = 0;
    private float totalTowerDamage = 0;
    private List<TowerSpot> towerSpots = new List<TowerSpot>();
    private List<TowerSpot> totalTowerSpots = new List<TowerSpot>();
    private List<PowersBase.PowerType> powersList = new List<PowersBase.PowerType>();
    private int powersCount = 0;
    private int enemiesHitByPowerCount = 0;
    private int notDefeatedEnemiesCount = 0;
    private int usedCoinsCount = 0;
    private int wavesCount = 0;
    private float timer = 0;
    private int attempts = 0;
    private bool usedSoldier = false;
    private List<Historic> historic = new List<Historic>();
    private int life = 0;

    private ExcelWorksheet worksheet = null;

    private string _filePath => Application.dataPath + "/../" + SceneManager.GetActiveScene().name + ".xlsx";
    private string _sheetName => "Tentativa" + attempts;

    public bool UsedSoldier => usedSoldier;
    public int AddTowerCount => addTowerCount;
    public int DiffTowersCount => towerTypes.Count;
    public float DamageScore =>  usedCoinsCount <= 0 ? 0 : totalTowerDamage / usedCoinsCount;
    public float PowersHit => powersCount <= 0 ? 0 : enemiesHitByPowerCount / powersCount;
    public float PositioningScore => HigherPositioningScore();
    public float MeleeSurrondingScore => HigherMeleeSurroundingsScore();
    public int CoinsUsed => usedCoinsCount;
    public int Life { set => life = value; }

    private void Start()
    {
        attempts = PlayerPrefs.GetInt(SceneManager.GetActiveScene().name + "attempts");

        attempts++;

        PlayerPrefs.SetInt(SceneManager.GetActiveScene().name + "attempts", attempts);
    }

    private void Update()
    {
        timer += Time.deltaTime;    
    }

    public void OnAddTower(TowerType towerType, TowerSpot towerSpot) 
    {
        //Number of towers and different towers
        addTowerCount++;
        towersCount++;

        if ((!towerTypes.Contains(towerType.TypeOfTower))) towerTypes.Add(towerType.TypeOfTower);

        //Coins used
        usedCoinsCount += towerType.BuyPrice;

        // Tower spots used and positioning scores
        if (!towerSpots.Contains(towerSpot)) towerSpots.Add(towerSpot);
        if (!totalTowerSpots.Contains(towerSpot)) totalTowerSpots.Add(towerSpot);

        UpdatePositioningScore();

        //Soldier tower surrondings
        UpdateMeleeSurroundingsScore();

        Historic newHistoric = new Historic(timer, towerPositioningScore, meleeTowerSurrondingScore, towersCount);
        historic.Add(newHistoric);
    }

    public void OnSellTower(TowerSpot towerSpot) 
    {
        sellTowerCount++;
        towersCount--;
        towerSpots.Remove(towerSpot);

        UpdatePositioningScore();
        if (towerSpot.IsMelee) UpdateMeleeSurroundingsScore();

        Historic newHistoric = new Historic(timer, towerPositioningScore, meleeTowerSurrondingScore, towersCount);
        historic.Add(newHistoric);
    }

    public void OnPowerUse(PowersBase.PowerType powerType, int enemiesHit) 
    {
        powersCount++;
        enemiesHitByPowerCount += enemiesHit;
        if (!powersList.Contains(powerType)) powersList.Add(powerType);
    }

    public void OnTowerDamage(float damage) 
    {
        totalTowerDamage += damage;
    }

    public void OnEnemyCrossedDefense() 
    {
        notDefeatedEnemiesCount++;
    }

    public void OnNewWave(int value)
    {
        wavesCount = value; 
    }

    private void UpdatePositioningScore()
    {
        towerPositioningScore = 0;
        foreach (TowerSpot towerSpot in towerSpots)
        {
            towerPositioningScore += towerSpot.PositioningScore;
        }

    }

    private void UpdateMeleeSurroundingsScore()
    {
        usedSoldier = true;
        float meleeCount = 0;
        float surrondingTowersCount = 0;
        meleeTowerSurrondingScore = 0;
        foreach (TowerSpot towerSpot in towerSpots)
        {
            if (towerSpot.IsMelee)
            {
                meleeCount++;
                surrondingTowersCount += towerSpot.MeleeSurrondingCount;
            }
        }
        meleeTowerSurrondingScore = meleeCount == 0 ? 0 : surrondingTowersCount / meleeCount;
    }

    private float HigherPositioningScore()
    {
        float score = 0;
        foreach(Historic h in historic)
        {
            if (h.positioningScore > score) score = h.positioningScore;
        }

        return score;
    }

    private float HigherMeleeSurroundingsScore()
    {
        float score = 0;
        foreach (Historic h in historic)
        {
            if (h.meleeSurrondingScore > score) score = h.meleeSurrondingScore;
        }

        return score;
    }

    private void OnDestroy()
    {
        WriteSpreadsheet();
    }

    private void WriteSpreadsheet()
    {   
        FileInfo _excelName = new FileInfo(_filePath);

        //adopt ExcelPackage Open the file
        using (ExcelPackage package = new ExcelPackage(_excelName))
        {
            if(worksheet != null) package.Workbook.Worksheets.Delete(worksheet.Index);

            //stay excel Empty File Added New sheet，And set the name.
            worksheet = package.Workbook.Worksheets.Add(_sheetName);
            worksheet.DefaultColWidth = 40;

            //Timer
            worksheet.Cells[1, 1].Value = "Tempo total";
            worksheet.Cells[1, 2].Value = Mathf.CeilToInt(timer);

            //Waves
            worksheet.Cells[2, 1].Value = "Ondas Enfrentadas";
            worksheet.Cells[2, 2].Value = wavesCount;

            //Different Towers
            worksheet.Cells[3, 1].Value = "Diferentes torres adicionadas";
            worksheet.Cells[3, 2].Value = towerTypes.Count;

            //Powera used
            worksheet.Cells[4, 1].Value = "Poderes utilizados";
            worksheet.Cells[4, 2].Value = powersCount;
            worksheet.Cells[5, 1].Value = "Diferentes poderes utilizados";
            worksheet.Cells[5, 2].Value = powersList.Count;

            //Tower buy and sell
            worksheet.Cells[6, 1].Value = "Torres adicionadas";
            worksheet.Cells[6, 2].Value = addTowerCount;
            worksheet.Cells[7, 1].Value = "Torres vendidas";
            worksheet.Cells[7, 2].Value = sellTowerCount;

            //Different TowerSpots used
            worksheet.Cells[8, 1].Value = "Espaços de torres diferentes utilizados";
            worksheet.Cells[8, 2].Value = totalTowerSpots.Count;

            //Towers damage / used coins
            worksheet.Cells[9, 1].Value = "Dano das torres/Moedas gastas";
            worksheet.Cells[9, 2].Value = DamageScore;

            //Hit enemies / used powers
            worksheet.Cells[10, 1].Value = "Inimigos Atingidos/Poderes utilizados";
            worksheet.Cells[10, 2].Value = PowersHit;

            //Not defended enemies
            worksheet.Cells[11, 1].Value = "Inimigos não derrotados";
            worksheet.Cells[11, 2].Value = notDefeatedEnemiesCount;

            //Not defended enemies
            worksheet.Cells[12, 1].Value = "Vida com que finalizou a fase";
            worksheet.Cells[12, 2].Value = life;

            //Historic Log
            worksheet.Cells[14, 1].Value = "Histórico";

            worksheet.Cells[15, 1].Value = "Tempo";
            worksheet.Cells[15, 2].Value = "Score do posicionamento de torres";
            worksheet.Cells[15, 3].Value = "Torres rodeando/Torres corpo a corpo";
            worksheet.Cells[15, 4].Value = "Número de torres";

            int historicFirstRow = 16;

            for(int i = 0; i< historic.Count; i++)
            {
                worksheet.Cells[historicFirstRow + i, 1].Value = Mathf.CeilToInt(historic[i].timer);
                worksheet.Cells[historicFirstRow + i, 2].Value = historic[i].positioningScore;
                worksheet.Cells[historicFirstRow + i, 3].Value = historic[i].meleeSurrondingScore;
                worksheet.Cells[historicFirstRow + i, 4].Value = historic[i].towerCount;
            }

            //Preservation excel
            package.Save();
        }
    }

    private struct Historic
    {
        public Historic(float timer, float positioningScore, float meleeSurrondingScore, int towerCount)
        {
            this.timer = timer;
            this.positioningScore = positioningScore;
            this.meleeSurrondingScore = meleeSurrondingScore;
            this.towerCount = towerCount;
        }

        public float timer;
        public float positioningScore;
        public float meleeSurrondingScore;
        public int towerCount;
    }
}
