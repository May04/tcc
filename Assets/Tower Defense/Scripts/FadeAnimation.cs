﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeAnimation : MonoBehaviour
{
    [SerializeField]
    private float duration = 0;
    [SerializeField]
    private float LifeTime = 0;
    [SerializeField]
    private Renderer componentRenderer = null;
    [SerializeField]
    private bool deactivateParent = false;

    private float timer = 0;

    private void OnEnable()
    {        
        timer = 0;
        StartCoroutine(FadeIn());
    }

    private void Update()
    {
        timer += Time.deltaTime;

        if(timer > LifeTime) StartCoroutine(FadeOut());
    }

    private IEnumerator FadeIn()
    {
        if (componentRenderer != null)
        {
            Color color = GetColor();           

            float time = 0;
            float startValue = 0;
            float endValue = 1;

            while (time <= duration)
            {
                color.a =  Mathf.Lerp(startValue, endValue, time / duration);
                time += Time.deltaTime;

                SetColor(color);
                yield return null;
            }
        }
    }

    private IEnumerator FadeOut()
    {
        if (componentRenderer != null)
        {
            Color color = GetColor();

            float time = 0;
            float startValue = 1;
            float endValue = 0;

            while (time <= duration)
            {
                color.a = Mathf.Lerp(startValue, endValue, time / duration);
                time += Time.deltaTime;

                SetColor(color);
                yield return null;
            }

            if(deactivateParent) transform.parent.gameObject.SetActive(false);
            else gameObject.SetActive(false);
        }
    }

    private Color GetColor()
    {
        if (componentRenderer.material.HasProperty("_Color")) return componentRenderer.material.GetColor("_Color");

        if (componentRenderer.material.HasProperty("_FaceColor")) return componentRenderer.material.GetColor("_FaceColor");

        return new Color();
    }

    private void SetColor(Color color)
    {
        if (componentRenderer.material.HasProperty("_Color")) componentRenderer.material.SetColor("_Color", color);

        if (componentRenderer.material.HasProperty("_FaceColor")) componentRenderer.material.SetColor("_FaceColor", color);

    }
}
