﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private Level[] levels = null;

    static GameManager instance = null;

    private Save save = new Save();

    private void Awake()
    {
        if (instance == null) instance = this;
        else Destroy(this.gameObject);

        SceneManager.sceneLoaded += LoadLevelData;

        DontDestroyOnLoad(this.gameObject);
    }

    private void OnDestroy()
    {
        SaveLevelData();
    }

    private void LoadLevelData(Scene scene, LoadSceneMode loadSceneMode)
    {
        if (File.Exists(Application.dataPath + "save.json"))
        {
            save = JsonUtility.FromJson<Save>(File.ReadAllText(Application.dataPath + "save.json"));

            for (int i = 0; i < levels.Length; i++)
            {
                foreach (Level level1 in save.levels)
                {
                    if (levels[i].name == level1.name)
                    {
                        if(level1.data != null) levels[i] = level1;
                    }
                }
               
                levels[i].data.OnValuesChanged?.Invoke();
            }
        }
        else
        {
            SaveLevelData();
            LoadLevelData(new Scene(), new LoadSceneMode());
        }
    }

    public void SaveLevelData()
    {
        save = new Save();
        foreach (Level level in levels)
        {
            save.levels.Add(level);
        }

        string json = JsonUtility.ToJson(save);

        File.WriteAllText(Application.dataPath + "save.json", json);
    }

    [System.Serializable]
    private struct Level
    {
        public string name;
        public LevelData data;
    }

    private class Save
    {
        public List<Level> levels = new List<Level>();
    }
}
