﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefenseSpot : MonoBehaviour
{
    [SerializeField]
    private PlayerStats playerStats = null;
    [SerializeField]
    private GameObject lostLifePrefab = null;
    [SerializeField]
    private Transform loseLifePos = null;
    private GainedOrLostStatIndicator lostLife = null;
    private Evaluation evaluation = null;
    private AudioSource audioSource = null;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();        
        evaluation = FindObjectOfType<Evaluation>();    
    }
    
    private void OnTriggerExit2D(Collider2D collision)
    {
        EnemyBase enemy = collision.gameObject.GetComponent<EnemyBase>();

        if (enemy != null)
        {
            if (enemy.Died) return;

            if (audioSource != null) audioSource.Play();

            playerStats.DecreaseHeartsIn(enemy.DamageToPlayerLife);

            lostLife = Instantiate(lostLifePrefab, loseLifePos).GetComponent<GainedOrLostStatIndicator>();
            lostLife.ValueMinusText = enemy.DamageToPlayerLife;
            lostLife.DestroyOnDisable = true;

            if (evaluation != null) evaluation.OnEnemyCrossedDefense();
            Destroy(enemy.gameObject);
        }
    }
}
