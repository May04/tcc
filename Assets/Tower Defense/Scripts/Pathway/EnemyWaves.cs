﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new EnemyWaves", menuName = "Tower Defense/Enemy/Waves")]
public class EnemyWaves : ScriptableObject
{
    public List<Wave> Waves = new List<Wave>();

    [System.Serializable]
    public struct Wave
    {
        public float Delay;
        public bool InfinityDelay;
        [TextArea]
        public string Description;
        public List<Enemy> Enemy;
    }

    [System.Serializable]
    public struct Enemy
    {
        public GameObject Prefab;
        public int Quantity;
        public float Delay;
    }
}
