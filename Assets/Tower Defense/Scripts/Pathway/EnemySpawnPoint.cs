﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnPoint : MonoBehaviour
{
    [SerializeField]
    private PlayerStats playerStats = null;
    [SerializeField]
    private WaveTimer waveTimerScript = null;
    [SerializeField]
    private int waveTimerTime = 0;
    [SerializeField]
    private EnemyWaves enemyWaves = null;
    [SerializeField]
    private List<Transform> targetPoints = new List<Transform>();
    [SerializeField]
    private bool advanceIfNoEnemy = true;
    [SerializeField]
    private bool advanceWithOtherAdvance = true;
    [SerializeField]
    private bool notCountWave = false;
    private int currentWaveCount = 0;
    private float waveTimer = 0;
    private float currentWaveDelay = 0;
    private bool waveCompleted = true;
    private bool waveAdvanced = false;
    private bool allCompleted = false;
    [SerializeField]
    private AudioSource advanceWaveAudio = null;

    private EnemyWaves.Wave currentWave =>  enemyWaves.Waves[currentWaveCount];

    public bool Completed => allCompleted;

    private void Start()
    {
        waveTimerScript.gameObject.SetActive(false);
        waveTimerScript.OnWaveAdvance += AdvanceWave;
        waveTimerScript.AnimationTime = waveTimerTime;
        playerStats.OnWavesChange += OtherWaveAdvanced;
        currentWaveDelay = currentWave.Delay;
        waveTimer = waveTimerTime;
    }

    private void Update()
    {
        if (enemyWaves.Waves.Count <= currentWaveCount)
        {
            allCompleted = true;
            return;
        }

        if (!waveCompleted) return;

        currentWaveDelay -= Time.deltaTime;

        if ((currentWaveDelay <= waveTimerTime && !waveAdvanced) || currentWave.InfinityDelay)
        {
            waveTimerScript.WaveDescriptionText = currentWave.Description;
            waveTimerScript.gameObject.SetActive(true);

            waveTimer -= Time.deltaTime;
        }

        if (waveTimer <= 0 || currentWave.Delay == 0)
        {
            waveTimer = waveTimerTime;
            StartCoroutine(SpawnWave());
            if(!notCountWave) playerStats.AdvanceWave(currentWaveCount + 1);
        }

        if (FindObjectsOfType<EnemyBase>().Length <= 0 && advanceIfNoEnemy)
        {
            currentWaveDelay = waveTimerTime;
        }

    }

    private IEnumerator SpawnWave()
    {
        waveCompleted = false;
        waveTimerScript.gameObject.SetActive(false);

        advanceWaveAudio.Play();

        int enemyWaveCount = 0;
        while (enemyWaveCount < currentWave.Enemy.Count)
        {
            int enemyCount = 0;
            while (enemyCount < currentWave.Enemy[enemyWaveCount].Quantity)
            {
                SpawnEnemy(enemyWaveCount);

                yield return new WaitForSeconds(currentWave.Enemy[enemyWaveCount].Delay);

                enemyCount++;
            }
            enemyWaveCount++;
        }

        currentWaveCount++;
        waveCompleted = true;
        waveAdvanced = false;
        waveTimer = waveTimerTime;

        if (currentWaveCount >= enemyWaves.Waves.Count) yield break;
        currentWaveDelay = currentWave.Delay;
    }

    private void SpawnEnemy(int index)
    {
        EnemyBase newEnemy = Instantiate(currentWave.Enemy[index].Prefab).GetComponent<EnemyBase>();
        newEnemy.transform.position = transform.position;
        newEnemy.transform.rotation = targetPoints[0].rotation;

        foreach (Transform target in targetPoints) newEnemy.targetPoints.Add(target);
    }

    private void AdvanceWave()
    {
        if (currentWaveCount >= enemyWaves.Waves.Count) return;

        int diamonds = Mathf.CeilToInt(waveTimer);
        playerStats.IncreaseDiamondsIn(diamonds);
        waveTimerScript.ShowGainedDiamonds(diamonds);
        waveTimer = 0;
        waveTimerScript.gameObject.SetActive(false);
        waveAdvanced = true;
        if (!notCountWave) playerStats.AdvanceWave(currentWaveCount + 1);
    }

    private void OtherWaveAdvanced(int value) 
    {
        if (!waveAdvanced && advanceWithOtherAdvance) AdvanceWave();
    }
}
