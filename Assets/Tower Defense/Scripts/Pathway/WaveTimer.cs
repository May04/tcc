﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class WaveTimer : Clickable
{
    [SerializeField]
    private TextMeshPro waveDescription = null;
    [SerializeField]
    private SpriteRenderer fill = null;
    [SerializeField]
    private GainedOrLostStatIndicator gainedDiamonds = null;
    [SerializeField]
    private bool infinity = false;
    private int animationTime = 0;
    public string WaveDescriptionText { set => waveDescription.text = value; }
    public int AnimationTime { set => animationTime = value; }

    public System.Action OnWaveAdvance;

    private void OnEnable()
    {
        waveDescription.transform.parent.gameObject.SetActive(false);
        gainedDiamonds.gameObject.SetActive(false);
        StopAllCoroutines();

        fill.sharedMaterial.SetFloat("_Arc1", 0);

        if (!infinity) StartCoroutine(AnimateFill());
    }

    public override void OnHover()
    {
        base.OnHover();
        waveDescription.transform.parent.gameObject.SetActive(true);
    }

    public override void OnHoverExit()
    {
        base.OnHoverExit();
        waveDescription.transform.parent.gameObject.SetActive(false);
    }

    public override void OnClick()
    {
        base.OnClick();
        OnWaveAdvance?.Invoke();
    }

    private IEnumerator AnimateFill()
    {
        if (animationTime > 0)
        {
            fill.sharedMaterial.SetFloat("_Arc1", 0);
            float time = 0;
            float startValue = 0;
            float endValue = 360;

            while (time <= animationTime)
            {
                fill.sharedMaterial.SetFloat("_Arc1", Mathf.Lerp(startValue, endValue, time / animationTime));
                time += Time.deltaTime;
                yield return null;
            }
        }
    }

    public void ShowGainedDiamonds(int value)
    {
        gainedDiamonds.ValuePlusText = value;
        gainedDiamonds.gameObject.SetActive(true);
    }
}
